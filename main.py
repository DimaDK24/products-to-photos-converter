import json
import os
from time import sleep

import requests
# import sys
from decouple import config

photo_description_format = """{title}

Цена: {price}

{desc}"""

access_token = config('access_token', default=None)
public_id = config('public_id', default=None)
captcha_key = config('rucaptcha_key', default=None)
if public_id and public_id[0] != '-':
    public_id = '-' + public_id

stop = False
print('Поддержка:', 't.me/dimadk24', 'vk.com/dimadk24', 'vk.com/id159204098', sep='\n', end='\n\n')
if not access_token:
    print('Отсутствует access token')
    # sys.exit()
    stop = True
if not public_id:
    print('Отсутствует ID паблика')
    # sys.exit()
    stop = True
if not captcha_key:
    print('Отсутствует ключ доступа к сервису решения капчи')
    # sys.exit()
    stop = True

api_url = 'https://api.vk.com/method/'
api_version = '5.71'


class MarketAlbum:
    id = None
    title = None
    product_count = None
    product_list = []

    def __init__(self, id, title, product_count):
        self.id = id
        self.title = title
        self.product_count = product_count
        self.product_list = []
        self.get_products()

    def __repr__(self):
        return self.title

    def get_products(self):
        json = request(url=api_url + 'market.get', owner_id=public_id, count=200, album_id=self.id, extended=1)
        if json['count'] > 200:
            print('Слишком много товаро в подборке (>200). Скрипт не работает на таких объемах.')
            # sys.exit()
        for item in json['items']:
            if item['availability'] == 0:  # If item availible
                photos = []
                for photo in item['photos']:
                    max_photo_url = get_max_photo_size(photo)
                    photos.append(max_photo_url)
                self.product_list.append(Product(item['id'], item['title'], item['description'], item['price']['text'],
                                                 photos, self))
        return self.product_list

    def export2photo_album(self):
        photo_album = PhotoAlbum(self.title)
        photo_list = []
        for product in self.product_list:
            photo_id = 0
            for photo_url in product.photo_list:
                product_photo_id = str(product.id) + "_" + str(photo_id)
                photo_list.append(Photo(photo_url,
                                        product_desc2photo_desc(product.title, product.description, product.price),
                                        photo_album, product_photo_id))
                photo_id += 1
        photo_album.photo_list = photo_list
        return photo_album


class Product:
    id = None
    title = None
    description = None
    price = None
    photo_list = []
    album = None

    def __init__(self, id, title, description, price, photo_list, album):
        self.id = id
        self.title = title
        self.description = description
        self.price = price
        self.photo_list = photo_list
        self.album = album

    def __repr__(self):
        return 'Товар "' + self.title + '". Цена: ' + self.price + ' Количество фото: ' + len(self.photo_list).__str__()


class PhotoAlbum:
    id = None
    title = None
    desc = None
    photo_list = []

    def __init__(self, title, id=None):
        self.title = title
        self.id = id
        self.upload_url = None

    def __repr__(self):
        return self.title

    def create(self):
        json = request(url=api_url + 'photos.createAlbum', group_id=abs(int(public_id)), title=self.title,
                       description=self.desc, upload_by_admins_only=1, comments_disabled=0)
        self.id = json['id']
        return self.id

    def save_data(self):
        data = []
        for photo in self.photo_list:
            data.append({photo.product_photo_id: photo.id})
        text_data = json.dumps(data)
        with open('Data.json', 'w') as f:
            f.write(text_data)

    def get_upload_server(self):
        json = request(url=api_url + 'photos.getUploadServer', album_id=self.id, group_id=abs(int(public_id)))
        self.upload_url = json['upload_url']
        return self.upload_url


class Photo:
    id = None
    descprition = None
    url = None
    album = None
    vk_desc = None

    def __init__(self, url, description, album, product_photo_id):
        self.url = url
        self.descprition = description
        self.album = album
        self.id = None
        self.product_photo_id = product_photo_id
        self.vk_desc = None

    def __repr__(self):
        return 'Фото "' + self.url + '" Описание: "' + self.descprition

    def upload(self):
        r = requests.get(self.url)
        extension = self.url[-3:]
        with open('temp.' + extension, 'wb') as f:
            f.write(r.content)
        r = requests.post(self.album.upload_url, files={'file1': open('temp.' + extension, 'rb')})
        os.remove('temp.' + extension)
        json_response = r.json()
        if json_response['server'] and json.loads(json_response['photos_list']) and json_response['hash']:
            return json_response
        else:
            return self.upload()

    def save(self, server, photos_list, hash):
        json = request(api_url + 'photos.save', album_id=self.album.id, group_id=abs(int(public_id)), server=server,
                       photos_list=photos_list, hash=hash, caption=self.descprition)
        self.id = json[0]['id']
        return self.id

    def update_description(self):
        json = request(url=api_url + 'photos.edit', owner_id=public_id, photo_id=self.id, caption=self.descprition)
        return json  # returns 1 if success


def request(url, **kwargs):
    working_data = kwargs.copy()
    working_data.update({'access_token': access_token, 'v': api_version, 'lang': 'ru'})
    r = requests.post(url, data=working_data)
    if r.status_code != requests.codes.ok:
        print('Ошибка ответа вк. Жду 5 секунд и повторяю запрос.')
        sleep(5)
        return request(url, **kwargs)
    json = r.json()
    if json.get('error'):
        if 'captcha_sid' in kwargs:
            kwargs.pop('captcha_sid')
        if 'captcha_key' in kwargs:
            kwargs.pop('captcha_key')
        return handle_error(json['error'], url, kwargs)
    else:
        return json['response']


def handle_error(json, url, kwargs):
    errors = {
        1: 'Произошла неизвестная ошибка',
        5: 'Авторизация пользователя не удалась',
        6: 'Слишком много запросов в секунду',
        8: 'Неверный запрос',
        9: 'Слишком много однотипных действий',
        10: 'Произошла внутренняя ошибка сервера',
        14: 'Капча. Решаю ее',
        15: 'Доступ запрещен',
        200: 'Доступ к альбому запрещён',
        203: 'Доступ к группе запрещён',
        300: 'Альбом переполнен'
    }
    error_code = json['error_code']
    try:
        error_rus_text = errors[error_code]
        if error_code != 6:
            print(error_rus_text)
    except KeyError:
        print('Произошла ошибка, отстутствующая в базе ошибок')
        print(json)  # Вывод неизвестных ошибок
    if error_code == 14:
        return handle_captcha_error(json, url, kwargs)
    else:
        return request(url=url, **kwargs)


def handle_captcha_error(response, url, request_kwargs):
    img_url = response['captcha_img']
    captcha_sid = response['captcha_sid']
    r = requests.get(img_url)
    with open('temp_captcha.jpg', 'wb') as f:
        f.write(r.content)
    while True:
        r = requests.post('http://rucaptcha.com/in.php',
                          data={'key': captcha_key, 'json': 1,
                                'method': 'post', 'regsense': 1, 'numeric': 4},
                          files={'file': open('temp_captcha.jpg', 'rb')})
        if r.status_code != requests.codes.ok:
            print('Упал сервис по решению капчи, к сожалению придется подождать')
            sleep(20)
        else:
            break
    os.remove('temp_captcha.jpg')
    json = r.json()
    if json['status']:
        captcha_id = json['request']
        sleep(5)
        while True:
            r = requests.get('http://rucaptcha.com/res.php', params={'key': captcha_key, 'action': 'get',
                                                                     'id': captcha_id, 'json': 1})
            if r.status_code != requests.codes.ok:
                print('Упал сервис по решению капчи, к сожалению придется подождать')
                sleep(20)
                continue
            json = r.json()
            if not json['status']:
                handle_captcha_service_error_response = handle_captcha_service_error(json['request'])
                if not handle_captcha_service_error_response:  # if error is not in nice errors list
                    save_and_exit()
                elif handle_captcha_service_error_response == 2:  # NO_SLOT_AVALIABLE
                    return request(url=url, **request_kwargs)
                sleep(5)
            else:
                break
        print('Капча решена, повторяю завпрос')
        captcha_text = json['request']
        return request(url=url, **request_kwargs, captcha_sid=captcha_sid, captcha_key=captcha_text)
    else:
        if not handle_captcha_service_error(json['request']):  # if error is not in nice errors list
            save_and_exit()
        return request(url=url, **request_kwargs)


def handle_captcha_service_error(error_text):
    errors = {
        'ERROR_WRONG_USER_KEY': 'Неверный ключ доступа к сервису решения капч',
        'ERROR_KEY_DOES_NOT_EXIST': 'Неверный ключ доступа к сервису решения капч',
        'ERROR_ZERO_BALANCE': 'Недостаточно денег на счете сервиса решения капч',
        'ERROR_NO_SLOT_AVAILABLE': 'Повысь ставку за решение капчи, или подожди',
        'ERROR_ZERO_CAPTCHA_FILESIZE': 'Ошибка с файлом капчи',
        'ERROR_TOO_BIG_CAPTCHA_FILESIZE': 'Ошибка с файлом капчи',
        'ERROR_WRONG_FILE_EXTENSION': 'Ошибка с файлом капчи',
        'ERROR_IMAGE_TYPE_NOT_SUPPORTED': 'Ошибка с файлом капчи',
        'ERROR_IP_NOT_ALLOWED': 'Проверь список адресов: https://rucaptcha.com/iplist',
        'IP_BANNED': 'IP-адрес забанен, обратись в поддержку сервиса или ко мне',
        'MAX_USER_TURN': 'Слишком частые обращения к сервису решения капч',
        'CAPCHA_NOT_READY': 'Капча еще не решена. Жду 5 секунд',
        'ERROR_CAPTCHA_UNSOLVABLE': 'Капча нерешаема, повторяю запрос',
        'ERROR_WRONG_ID_FORMAT': 'Неверный формат id капчи',
        'ERROR_WRONG_CAPTCHA_ID': 'Неверный id капчи',
        'ERROR: 1001': 'Сервис по решению капчи заблокировал нас на 10 мин, подожди их',
        'ERROR: 1002': 'Сервис по решению капчи заблокировал нас на 5 мин, подожди их',
        'ERROR: 1003': 'Сервис по решению капчи заблокировал нас на 30 сек, подожди их',
        'ERROR: 1004': 'Сервис по решению капчи заблокировал нас на 10 мин, подожди их',
        'ERROR: 1005': 'Сервис по решению капчи заблокировал нас на 5 мин, подожди их'
    }
    try:
        rus_error_text = errors[error_text]
        print(rus_error_text)
    except KeyError:
        print('Неизвестная ошибка сервиса решения капч')
    if error_text == 'CAPCHA_NOT_READY' or error_text == 'ERROR_NO_SLOT_AVAILABLE':
        return 1
    elif error_text == 'ERROR_CAPTCHA_UNSOLVABLE':
        return 2
    else:
        return 0


def get_market_albums():
    json = request(url=api_url + 'market.getAlbums', owner_id=public_id, count=100)
    if json['count'] > 100:
        print('Слишком много подборок (>100). Скрипт не работает на таких объемах.')
        # sys.exit()
    albums = []
    for item in json['items']:
        albums.append(MarketAlbum(item['id'], item['title'], item['count']))
    return albums


def get_photo_albums():
    """
    :returns {'title': id}
    """
    json = request(url=api_url + 'photos.getAlbums', owner_id=public_id)
    albums = {}
    for item in json['items']:
        albums.update({item['title']: item['id']})
    return albums


def product_desc2photo_desc(title, desc, price):
    photo_description = photo_description_format.format(title=title, desc=desc.strip(), price=price)
    if len(photo_description) <= 2048:
        return photo_description_format.format(title=title.strip(), desc=desc.strip(), price=price)
    else:
        print('Слишком большое описание товара', title)
        return photo_description_format.format(title=title.strip(), desc=desc.strip()[:1800], price=price)


def product_albums2photo_album(product_albums):
    photo_albums = []
    for product_album in product_albums:
        photo_albums.append(product_album.export2photo_album())
    return photo_albums


def save_albums_data(photo_albums):
    data = {}
    for photo_album in photo_albums:
        for photo in photo_album.photo_list:
            if photo.id:
                if photo.product_photo_id in data:
                    data[photo.product_photo_id].update({photo_album.title: photo.id})
                else:
                    data.update({photo.product_photo_id: {photo_album.title: photo.id}})
    with open('Data.json', 'w') as f:
        f.write(json.dumps(data))


def get_photo_desc(uploaded_photos_dict):
    lists_of_photo_ids = list(uploaded_photos_dict.values())
    all_photo_id_list = []
    for list_of_photo_ids in lists_of_photo_ids:
        for photo_id in list(list_of_photo_ids.values()):
            all_photo_id_list.append(photo_id)
    descriptions = {}
    for i in range(0, len(all_photo_id_list), 300):
        string = ''
        for photo_id in all_photo_id_list[i:i + 300]:
            string += str(public_id) + '_' + str(photo_id) + ','
        json = request(url=api_url + 'photos.getById', photos=string)
        for item in json:
            descriptions.update({item['id']: item['text']})
    return descriptions


def load_uploaded_photos():
    """
    :returns {"product_id_photo_id": {"album_title": photo_id}}
    """
    if os.path.exists('Data.json'):
        with open('Data.json', 'r') as f:
            data = f.read()
            f.close()
        return json.loads(data)
    else:
        return {}


def merge(photo_albums, created_albums):
    for photo_album in photo_albums:
        created_album_id = created_albums.get(photo_album.title)
        if created_album_id:
            photo_album.id = created_album_id
    return photo_albums


def save_and_exit():
    if photo_albums:
        save_albums_data(photo_albums)
    sleep(7200)
    # sys.exit()


def get_max_photo_size(photo):
    sizes = [75, 130, 604, 807, 1280, 2560]
    sizes.reverse()
    for size in sizes:
        try:
            return photo['photo_' + str(size)]
        except KeyError:
            pass


if not stop:
    product_albums = get_market_albums()
    photo_albums = product_albums2photo_album(product_albums)
    del product_albums
    print('Альбомы с фото, полученные из подборок товаров:', photo_albums)
    print('Количество альбомов, полученных из подборок товаров:', len(photo_albums))
    created_albums = get_photo_albums()
    uploaded_photos = load_uploaded_photos()
    if uploaded_photos:
        uploaded_photos_desc = get_photo_desc(uploaded_photos)
    else:
        uploaded_photos_desc = {}
    photo_albums = merge(photo_albums, created_albums)

    photo_count = 0
    for photo_album in photo_albums:
        photo_count += len(photo_album.photo_list)
    print('Всего фото:', photo_count)

    for photo_album in photo_albums:
        for photo in photo_album.photo_list:
            uploaded_photo = uploaded_photos.get(photo.product_photo_id)
            if uploaded_photo:
                uploaded_photo = uploaded_photo.get(photo_album.title)
                if uploaded_photo:
                    photo.id = uploaded_photo
                    try:
                        photo.vk_desc = uploaded_photos_desc[uploaded_photo]
                    except KeyError:
                        print('Ошибка получения описания фотки из вк. Не удаляй альбомы, загруженные этим скриптом.',
                              'Для решения ошибки удали файл Data.json, но все товары загружатся заново', sep='\n')

    overall_i = 1
    for photo_album in photo_albums:
        album_i = 1
        print('Альбом', photo_album.title, 'Количество фото:', len(photo_album.photo_list))
        if not photo_album.id:
            photo_album.create()
            print('Создан альбом:', photo_album.title)
        else:
            print('Альбом', photo_album.title, 'уже существует, не создаю')
        for photo in photo_album.photo_list:
            if not photo.id:
                if not photo_album.upload_url:
                    photo_album.get_upload_server()
                upload_data = photo.upload()
                photo_id = photo.save(upload_data['server'], upload_data['photos_list'], upload_data['hash'])
                if photo_id:
                    print('Фото {album_i} ({overall_i}) загружено'.format(overall_i=overall_i, album_i=album_i))
                else:
                    print('Фото {album_i} ({overall_i}) не загружено, возникла странная ошибка'.format(
                        overall_i=overall_i,
                        album_i=album_i))
            else:
                if photo.vk_desc != photo.descprition:
                    result = photo.update_description()
                    if result:
                        print('Фото {album_i} ({overall_i}) обновлено описание'.format(overall_i=overall_i,
                                                                                       album_i=album_i))
                    del result
                    # else:
                    # print('Фото {album_i} ({overall_i}) пропущено'.format(overall_i=overall_i, album_i=album_i))
            album_i += 1
            overall_i += 1
    save_albums_data(photo_albums)
    print('Все успешно загружено, окно можно закрыть')
    sleep(1800)
else:
    sleep(60)
